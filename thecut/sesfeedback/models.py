# -*_ coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from dateutil.parser import parse as parse_datetime
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from jsonfield import JSONField


models.options.DEFAULT_NAMES = models.options.DEFAULT_NAMES + ('type',)


@python_2_unicode_compatible
class Notification(models.Model):

    data = JSONField(editable=False)

    created_at = models.DateTimeField(auto_now_add=True, editable=False)

    class Meta(object):
        abstract = True
        get_latest_by = 'created_at'
        ordering = ['-created_at']
        type = None

    def __str__(self):
        return '{0}'.format(self.created_at)

    @classmethod
    def parse_notification(cls, notification):
        return cls.objects.create(data=notification)


@python_2_unicode_compatible
class BounceNotification(Notification):

    _BOUNCE_TYPE_DESCRIPTIONS = {
        # http://docs.aws.amazon.com/ses/latest/DeveloperGuide/notification-contents.html#bounce-types
        'Undetermined': {
            'Undetermined': 'Amazon SES was unable to determine a specific '
                            'bounce reason.',
            },
        'Permanent': {
            'General': 'Amazon SES received a general hard bounce and '
                       'recommends that you remove the recipient\'s email '
                       'address from your mailing list.',
            'NoEmail': 'Amazon SES received a permanent hard bounce because '
                       'the target email address does not exist. It is '
                       'recommended that you remove that recipient from your '
                       'mailing list.',
            'Suppressed': 'Amazon SES has suppressed sending to this address '
                          'because it has a recent history of bouncing as an '
                          'invalid address.',
            },
        'Transient': {
            'General': 'Amazon SES received a general bounce. You may be able '
                       'to successfully retry sending to that recipient in '
                       'the future.',
            'MailboxFull': 'Amazon SES received a mailbox full bounce. You '
                           'may be able to successfully retry sending to that '
                           'recipient in the future.',
            'MessageTooLarge': 'Amazon SES received a message too large '
                               'bounce. You may be able to successfully retry '
                               'sending to that recipient if you reduce the '
                               'message size.',
            'ContentRejected': 'Amazon SES received a content rejected '
                               'bounce. You may be able to successfully retry '
                               'sending to that recipient if you change the '
                               'message content.',
            'AttachmentRejected': 'Amazon SES received an attachment rejected '
                                  'bounce. You may be able to successfully '
                                  'retry sending to that recipient if you '
                                  'remove or change the attachment.',
            },
        }

    class Meta(Notification.Meta):
        type = 'Bounce'

    def __str__(self):
        return 'Email bounce ({0}: {1})'.format(self.type, self.sub_type)

    @property
    def description(self):
        return self._BOUNCE_TYPE_DESCRIPTIONS[self.type][self.sub_type]

    def get_email_addresses(self):
        return [recipient['emailAddress'] for recipient
                in self.data['bounce']['bouncedRecipients']]

    @property
    def sub_type(self):
        return self.data['bounce']['bounceSubType']

    @property
    def timestamp(self):
        return parse_datetime(self.data['bounce']['timestamp'])

    @property
    def type(self):
        return self.data['bounce']['bounceType']


@python_2_unicode_compatible
class ComplaintNotification(Notification):

    _COMPLAINT_TYPE_DESCRIPTIONS = {
        # http://docs.aws.amazon.com/ses/latest/DeveloperGuide/notification-contents.html#complaint-types
        'abuse': 'Indicates unsolicited email or some other kind of email '
                 'abuse.',
        'auth-failure': 'Indicates unsolicited email or some other kind of '
                        'email abuse.',
        'fraud': 'Indicates some kind of fraud or phishing activity.',
        'not-spam': 'Indicates that the entity providing the report does not '
                    'consider the message to be spam. This may be used to '
                    'correct a message that was incorrectly tagged or '
                    'categorized as spam.',
        'other': 'Indicates any other feedback that does not fit into other '
                 'registered types.',
        'virus': 'Reports that a virus is found in the originating message.',
        }

    class Meta(Notification.Meta):
        type = 'Complaint'

    def __str__(self):
        if self.type:
            return 'Email complaint ({0})'.format(self.type)
        else:
            return 'Email complaint'

    @property
    def description(self):
        return self._COMPLAINT_TYPE_DESCRIPTIONS.get(self.type, None)

    def get_email_addresses(self):
        return [recipient['emailAddress'] for recipient
                in self.data['complaint']['complainedRecipients']]

    @property
    def timestamp(self):
        return parse_datetime(self.data['complaint']['timestamp'])

    @property
    def type(self):
        return self.data['complaint'].get('complaintFeedbackType', None)
