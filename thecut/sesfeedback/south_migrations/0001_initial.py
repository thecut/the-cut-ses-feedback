# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'BounceNotification'
        db.create_table(u'sesfeedback_bouncenotification', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('data', self.gf('jsonfield.fields.JSONField')(default={})),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'sesfeedback', ['BounceNotification'])

        # Adding model 'ComplaintNotification'
        db.create_table(u'sesfeedback_complaintnotification', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('data', self.gf('jsonfield.fields.JSONField')(default={})),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'sesfeedback', ['ComplaintNotification'])


    def backwards(self, orm):
        # Deleting model 'BounceNotification'
        db.delete_table(u'sesfeedback_bouncenotification')

        # Deleting model 'ComplaintNotification'
        db.delete_table(u'sesfeedback_complaintnotification')


    models = {
        u'sesfeedback.bouncenotification': {
            'Meta': {'ordering': "[u'-created_at']", 'object_name': 'BounceNotification'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'data': ('jsonfield.fields.JSONField', [], {'default': '{}'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'sesfeedback.complaintnotification': {
            'Meta': {'ordering': "[u'-created_at']", 'object_name': 'ComplaintNotification'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'data': ('jsonfield.fields.JSONField', [], {'default': '{}'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['sesfeedback']