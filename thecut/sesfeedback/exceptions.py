# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals


class BaseSESFeedbackError(Exception):

    pass


class NotificationTypeNotFound(BaseSESFeedbackError):
    """A model for the notification type was not found."""

    pass
