# -*_ coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from . import exceptions, settings, utils
from boto import sqs
from celery import shared_task
from celery.utils.log import get_task_logger
import json


logger = get_task_logger(__name__)


@shared_task(bind=True, serializer='json', ignore_result=True)
def fetch_messages(self):
    logger.info('Fetching messages in SQS feedback queues.')
    for region, queue in settings.SQS_QUEUES:
        fetch_messages_for_queue.delay(region, queue)


@shared_task(bind=True, serializer='json', ignore_result=True)
def fetch_messages_for_queue(self, region_name, queue_name):
    logger.info('Fetching messages in {0} queue.'.format(queue_name))
    connection = sqs.connect_to_region(region_name)
    queue = connection.get_queue(queue_name)
    queue.set_message_class(sqs.message.RawMessage)

    for message in queue.get_messages(settings.SQS_MESSAGES_COUNT):
        logger.info('Message found in {0} queue.'.format(queue_name))
        data = json.loads(message.get_body())
        notification = json.loads(data['Message'])
        parse_notification.delay(notification)
        queue.delete_message(message)


@shared_task(bind=True, serializer='json', ignore_result=True)
def parse_notification(self, notification):
    notification_type = notification.get('notificationType', None)
    logger.info('Attempting to parse "{0}" notification.'.format(
        notification_type))
    try:
        model = utils.get_notification_types()[notification_type]
    except KeyError:
        raise exceptions.NotificationTypeNotFound(json.dumps(notification))
    else:
        model.parse_notification(notification)
