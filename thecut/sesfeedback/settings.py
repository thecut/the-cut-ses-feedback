# -*_ coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from django.conf import settings


# Number of messages to fetch from the queue and process
SQS_MESSAGES_COUNT = getattr(settings, 'SES_FEEDBACK_SQS_MESSAGES_COUNT', 10)

# List of tuples in format (region_name, queue_name)
SQS_QUEUES = getattr(settings, 'SES_FEEDBACK_SQS_QUEUES', [])
