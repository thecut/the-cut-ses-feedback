# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from django.db.models import get_models


__all__ = ['get_notification_models', 'get_notification_types']


NOTIFICATION_MODELS = None

NOTIFICATION_TYPES = None


def get_notification_models():
    """Returns list of models which subclass Notification."""
    global NOTIFICATION_MODELS
    if NOTIFICATION_MODELS is None:
        from .models import Notification
        NOTIFICATION_MODELS = [model for model in get_models()
                               if issubclass(model, Notification)]
    return NOTIFICATION_MODELS


def get_notification_types():
    """Returns dict of sqs notification types with model values."""
    global NOTIFICATION_TYPES
    if NOTIFICATION_TYPES is None:
        types = [(model._meta.type, model) for model
                 in get_notification_models()]
        NOTIFICATION_TYPES = dict(types)
    return NOTIFICATION_TYPES
